//
//  AppDelegate.swift
//  TodoApp
//
//  Created by Rupert Ben Wiser on 10/05/2020.
//  Copyright © 2020 Rupert Ben Wiser. All rights reserved.
//

import Cocoa
import EventKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.squareLength)
    var todos = Set<String>()
    var eventStore: EKEventStore?

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if let button = statusItem.button {
          button.image = NSImage(named:NSImage.Name("Icon"))
        }
        constructMenu()
        eventStore = EKEventStore()
        requestEventPermission()
        
    }
    
    private func requestEventPermission() {
        eventStore?.requestAccess(to: EKEntityType.event) { (accessGranted, error) in
            print(accessGranted)
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        
    }

    func constructMenu() {
        let menu = NSMenu()
        menu.addItem(NSMenuItem(title: "Add Todo", action: #selector(addTodo(_:)), keyEquivalent: "n"))
        menu.addItem(NSMenuItem.separator())
        menu.addItem(NSMenuItem(title: "Quit Quotes", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q"))

        statusItem.menu = menu
    }
    
    @objc func addTodo(_ sender: Any?) {
        let alert: NSAlert = NSAlert()
        let textField = NSTextField(frame: NSMakeRect(20, 0, 300, 20))
        alert.alertStyle = .informational
        alert.icon = NSImage(named:NSImage.Name("Big icon"))
        alert.messageText = "What Todo would you like to add?"
        alert.accessoryView = textField
        alert.addButton(withTitle: "Add Todo")
        alert.addButton(withTitle: "Cancel")
        let response = alert.runModal()
        
        if (response == .alertFirstButtonReturn) {
            let todo = textField.stringValue
            if (todos.contains(todo)) {
                return
            }
            
            todos.insert(todo)
            
            let todoButton = NSMenuItem(title: todo, action: #selector(completeTodo(_:)), keyEquivalent: "")
            statusItem.menu?.insertItem(todoButton, at: 1)
            
            if (todos.count == 1) {
                statusItem.menu?.insertItem(NSMenuItem.separator(), at: 1)
            }
        }
    }
    
    @objc func completeTodo(_ sender: NSMenuItem) {
        statusItem.menu?.removeItem(sender)
        todos.remove(sender.title)
        
        let newEvent = EKEvent(eventStore: eventStore!)
        newEvent.calendar = eventStore?.defaultCalendarForNewEvents
        newEvent.title = sender.title
        newEvent.startDate = Date()
        newEvent.endDate = Date()
        newEvent.calendar = eventStore?.defaultCalendarForNewEvents
        do {
            try eventStore?.save(newEvent, span: .thisEvent)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

